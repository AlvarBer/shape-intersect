﻿using System;
using System.Collections.Generic;

namespace ShapeIntersect
{
    public class Program
    {
        // static void Main(string[] args)
        // {
        //     Circle circ = new Circle(0, 0, 1, 1);
        //     Segment seg = new Segment(0.5f, 0.5f, 1.5f, 1.5f, 2);
        //     Rectangle rect = new Rectangle(1, 1, 1, 1, 3);
        //     List<Shape> shapes = new List<Shape>();
        //     shapes.Add(circ);
        //     shapes.Add(seg);
        //     shapes.Add(rect);
        //     foreach(KeyValuePair<int, List<Shape>> entry in Shape.FindIntersections(shapes))
        //     {
        //         entry.Value.ForEach(otherShape => Console.WriteLine($"{entry.Key}: {otherShape}"));
        //     }
        // }

        public static void Main(string[] args)
        {
            Rectangle rectLeftUp = new Rectangle(10, 60, 15, 70, 1);
            Circle circLeftUp = new Circle(40, 90, 2, 2);
            Circle circLeftBot = new Circle(35, 30, 5, 3);
            Circle circRightUp = new Circle(60, 80, 5, 4);
            Segment segRightBot = new Segment(65, 20, 75, 30, 5);

            QuadTree<Shape> tree = new QuadTree<Shape>(new Rectangle(50, 50, 50, 50));
            tree.Insert(rectLeftUp);
            tree.Insert(circLeftUp);
            tree.Insert(circLeftBot);
            tree.Insert(circRightUp);
            tree.Insert(segRightBot);

            List<Shape> inRange = tree.Query(rectLeftUp);
            inRange.ForEach(s => Console.WriteLine($"{s}"));
            foreach(KeyValuePair<int, List<Shape>> entry in Shape.FindIntersections(inRange))
            {
                entry.Value.ForEach(other => Console.WriteLine($"{entry.Key}: {other}"));
            }
        }
    }
}
