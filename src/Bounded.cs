namespace ShapeIntersect
{
    public interface IBounded
    {
        Rectangle Bounds();
    }
}