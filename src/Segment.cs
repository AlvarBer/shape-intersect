using System;

namespace ShapeIntersect
{
    public class Segment : Shape
    {
        public Vector2 start;
        public Vector2 end;

        public Segment(float startX, float startY, float endX, float endY, int id)
        {
            this.start = new Vector2(startX, startY);
            this.end = new Vector2(endX, endY);
            this.id = id;
        }

        public Segment(Vector2 start, Vector2 end, int id)
        {
            this.start = start;
            this.end = end;
            this.id = id;
        }

        public Segment(Vector2 start, Vector2 end)
        {
            this.start = start;
            this.end = end;
        }

        // TODO: Explain/derive
        public static bool Collide(Segment segA, Segment segB)
        {
            float a1 = segA.end.y - segA.start.y;
            float b1 = segA.start.x - segA.end.x;
            float c1 = a1 * segA.start.x + b1 * segA.start.y;

            float a2 = segB.end.y - segB.start.y;
            float b2 = segB.start.x - segB.end.x;
            float c2 = a2 * segB.start.x + b2 * segB.start.y;

            float delta = a1 * b2 - a2 * b1;

            if (delta == 0)
            {
                return segA.GeneralForm().Equals(segB.GeneralForm()) &&
                    (segA.Contains(segB.start) || segA.Contains(segB.end));
            }
            else
            {
                Vector2 intersect = new Vector2((b2 * c1 - b1 * c2) / delta, (a1 * c2 - a2 * c1) / delta);
                return segA.Contains(intersect) && segB.Contains(intersect);
            }
        }

        /// Return null rather than NaN on vertical lines
        public float? Slope()
        {
            if (end.x - start.x != 0)
            {
                return (end.y - start.y) / end.x - start.x;
            }
            else
            {
                return null;
            }
        }

        public (float, float, float) GeneralForm()
        {
            float? slope = Slope();
            float a, b, c;
            if (slope.HasValue)
            {
                a = slope.Value == 0 ? 0 : 1;
                b = -(1 / slope.Value);
                c = start.x - start.y / slope.Value;
            }
            else
            {
                a = 1;
                b = 0;
                c = -start.x;
            }

            return (a, b, c);
        }

        /// https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
        public float DistanceToPoint(Vector2 point)
        {
            var (a, b, c) = GeneralForm();
            float distance = Math.Abs(a * point.x + b * point.y + c) / (float)Math.Sqrt((a * a + b * b));
            return distance;
        }

        /// Returns whether a point is contained in a segment assuming the point is colinear
        public bool Contains(Vector2 point)
        {
            return (
                point.x <= Math.Max(start.x, end.x) &&
                point.x >= Math.Min(start.x, end.x) &&
                point.y <= Math.Max(start.y, end.y) &&
                point.y >= Math.Min(start.y, end.y)
            );
        }

        public override Rectangle Bounds()
        {
            Vector2 halfDiagonal = (start - end) * 0.5f;
            Vector2 center = end + halfDiagonal;
            Vector2 extents = halfDiagonal.abs;
            return new Rectangle(center, extents);
        }

        public override string ToString() => $"{start}->{end}";
    }
}