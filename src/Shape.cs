using System;
using System.Collections.Generic;
using System.Linq;

namespace ShapeIntersect
{
    public abstract class Shape : IBounded
    {
        public int id;

        public static Dictionary<int, List<Shape>> FindIntersections(List<Shape> shapes)
        {
            Dictionary<int, List<Shape>> intersections = new Dictionary<int, List<Shape>>();

            foreach (var shape in shapes)
            {
                List<Shape> collidingShapes = new List<Shape>();
                foreach (var otherShape in shapes)
                {
                    if (shape != otherShape) {
                        Console.WriteLine($"{shape} // {otherShape} X {DoIntersect(shape, otherShape)}");
                    }
                    if (shape != otherShape && DoIntersect(shape, otherShape))
                    {
                        collidingShapes.Add(otherShape);
                    }
                }
                intersections.Add(shape.id, collidingShapes);
            }

            return intersections;
        }

        // Without Algebraic Data Types either we need to define multiple functions
        // per subclass for each parameter or we can pattern match on the types
        public static bool DoIntersect(Shape left, Shape right)
        {
            switch ((left, right))
            {
                case var x when x.Item1 is Circle c1 && x.Item2 is Circle c2:
                    return DoIntersect(c1, c2);
                case var x when x.Item1 is Rectangle r1 && x.Item2 is Rectangle r2:
                    return DoIntersect(r1, r2);
                case var x when x.Item1 is Segment s1 && x.Item2 is Segment s2:
                    return DoIntersect(s1, s2);
                case var x when x.Item1 is Circle c && x.Item2 is Segment s:
                    return DoIntersect(c, s);
                case var x when x.Item1 is Segment s && x.Item2 is Circle c:
                    return DoIntersect(c, s);
                case var x when x.Item1 is Circle c && x.Item2 is Rectangle r:
                    return DoIntersect(c, r);
                case var x when x.Item1 is Rectangle r && x.Item2 is Circle c:
                    return DoIntersect(c, r);
                case var x when x.Item1 is Rectangle r && x.Item2 is Segment s:
                    return DoIntersect(r, s);
                case var x when x.Item1 is Segment s && x.Item2 is Rectangle r:
                    return DoIntersect(r, s);
                default:
                    throw new ArgumentException("Unexpected shape");
            }
        }

        public static bool DoIntersect(Circle circA, Circle circB)
        {
            return Vector2.Distance(circA.center, circB.center) < circA.radius + circB.radius;
        }

        /// Assuming rectangles are axis aligned we can justcheck for corners
        public static bool DoIntersect(Rectangle rectA, Rectangle rectB)
        {
            return !(
                rectA.minCorner.y >= rectB.maxCorner.y ||
                rectB.minCorner.y >= rectA.maxCorner.y ||
                rectA.minCorner.x >= rectB.maxCorner.x ||
                rectB.minCorner.x >= rectA.maxCorner.x
            );
        }

        public static bool DoIntersect(Segment segA, Segment segB)
        {
            return Segment.Collide(segA, segB);
        }

        public static bool DoIntersect(Circle circ, Segment seg)
        {
            return seg.DistanceToPoint(circ.center) <= circ.radius;
        }

        public static bool DoIntersect(Circle circ, Rectangle rect)
        {
            return System.Array.Exists(rect.Sides(), side => side.DistanceToPoint(circ.center) <= circ.radius);
        }

        public static bool DoIntersect(Rectangle rect, Segment seg)
        {
            var sidesCollide = System.Array.Exists(rect.Sides(), side => Segment.Collide(side, seg));
            return rect.Contains(seg.start) || rect.Contains(seg.end) || sidesCollide;
        }

        public abstract Rectangle Bounds();
    }
}