using System;
using System.Collections.Generic;

namespace ShapeIntersect
{
    public class QuadTree<T>
    where T : IBounded
    {
        private uint capacity;
        private uint depth;
        private uint maxDepth;
        private List<T> elements;
        private Rectangle area;
        private QuadTree<T>[] children;

        public QuadTree(Rectangle area)
        {
            this.capacity = 4;
            this.depth = 0;
            this.maxDepth = 10;
            this.elements = new List<T>();
            this.area = area;
            this.children = null;
        }

        public QuadTree(Rectangle area, uint depth)
        {
            this.capacity = 4;
            this.depth = depth;
            this.maxDepth = 10;
            this.elements = new List<T>();
            this.area = area;
            this.children = null;
        }

        /// Adds a tree to the QuadTree, this is a recursive operation that
        // may split the tree further if necessary
        public void Insert(T element)
        {
            switch ((FitsQuad(element), children))
            {
                // If the item fits on a quadrant and there are children we recurse into
                // the appropiate child
                case var x when x.Item1 is Quadrant quad && x.Item2 is QuadTree<T>[] children:
                    children[(int)quad].Insert(element);
                    break;
                // If it doesn't fit and there are children we add it to ourselves,
                // this means the element is bigger than any of our quadrants
                case var x when x.Item1 is null && x.Item2 is QuadTree<T>[]:
                    elements.Add(element);
                    break;
                // There are no children, this only happens the first time we reach
                // a node, we add the element and split if necessary
                case var x when x.Item2 is null:
                    elements.Add(element);
                    if (elements.Count > capacity)
                    {
                        Split();
                    }
                    break;
            }
        }

        /// Returns a list of elements on the same quadrant and parents of the quad
        public List<T> Query(T element)
        {
            // Edge case where element is even outside tree root
            if (area.Contains(element.Bounds()))
            {
                return null;
            }
            
            List<T> inQuadrant = new List<T>();
            QuadTree<T> tree = this;
            Stack<QuadTree<T>> futureTrees = new Stack<QuadTree<T>>();
            inQuadrant.AddRange(tree.elements);

            while (tree.children != null && futureTrees.Count > 0)
            {
                switch ((tree.FitsQuad(element), tree.children))
                {
                    // If elements fits into a quad we set the current tree to the quad
                    case var x when x.Item1 is Quadrant quad && x.Item2 is QuadTree<T>[] children:
                        tree = children[(int) quad];
                        break;
                    // If element doesn't fit we add the children to our future list
                    case var x when x.Item1 is null && x.Item2 is QuadTree<T>[] children:
                        tree = tree.children[0];
                        futureTrees.Push(children[1]);
                        futureTrees.Push(children[2]);
                        futureTrees.Push(children[3]);
                        break;
                    // No children, we are on a leaf node, add previous future trees
                    default:
                        if (futureTrees.Count > 0)
                        {
                            tree = futureTrees.Pop();
                        }
                        break;
                }
            }
            return inQuadrant;
        }

        // Children won't be null by the time split is called
        private void Split()
        {
            if (depth == maxDepth)
            {
                return;
            }
            // We create a children for each of the quadrants clockwise
            // (NorthEast, SouthEast, SouthWest, NorthWest)
            QuadTree<T>[] children = {
                new QuadTree<T>(
                    new Rectangle(
                        new Vector2(
                            area.center.x + area.extents.x / 2,
                            area.center.y + area.extents.y / 2
                        ),
                        area.extents * 0.5f
                    ),
                    depth + 1
                ),
                new QuadTree<T>(
                    new Rectangle(
                        new Vector2(
                            area.center.x - area.extents.x / 2,
                            area.center.y + area.extents.y / 2
                        ),
                        area.extents * 0.5f
                    ),
                    depth + 1
                ),
                new QuadTree<T>(
                    new Rectangle(
                        new Vector2(
                            area.center.x + area.extents.x / 2,
                            area.center.y - area.extents.y / 2
                        ),
                        area.extents * 0.5f
                    ),
                    depth + 1
                ),
                new QuadTree<T>(
                    new Rectangle(
                        new Vector2(
                            area.center.x - area.extents.x / 2,
                            area.center.y - area.extents.y / 2
                        ),
                        area.extents * 0.5f
                    ),
                    depth + 1
                )
            };
            List<T> unfitElements = new List<T>();
            foreach (var element in elements)
            {
                if (FitsQuad(element).HasValue)
                {
                    children[(int) FitsQuad(element).Value].Insert(element);
                }
                else
                {
                    unfitElements.Add(element);
                }
            }

            this.children = children;
            this.elements = unfitElements;
        }

        private Quadrant? FitsQuad(T element)
        {
            Rectangle bounds = element.Bounds();

            bool fitsLeftHalf = bounds.minCorner.x >= area.minCorner.x &&
                bounds.maxCorner.x <= area.center.x;
            bool fitsRightHalf = bounds.minCorner.x >= area.center.x &&
                bounds.maxCorner.x <= area.maxCorner.x;
            bool fitsBotHalf = bounds.minCorner.y >= area.minCorner.y &&
                bounds.maxCorner.y <= area.center.y;
            bool fitsTopHalf = bounds.minCorner.y >= area.center.y &&
                bounds.maxCorner.y <= area.maxCorner.y;
            
            if (fitsTopHalf && fitsRightHalf)
            {
                return Quadrant.TopRight;
            }
            else if (fitsBotHalf && fitsRightHalf)
            {
                return Quadrant.BotRight;
            }
            else if (fitsBotHalf && fitsLeftHalf)
            {
                return Quadrant.BotLeft;
            }
            else if (fitsTopHalf && fitsLeftHalf)
            {
                return Quadrant.TopLeft;
            }
            else
            {
                return null;
            }
        }

        private enum Quadrant
        {
            TopRight,
            BotRight,
            BotLeft,
            TopLeft,
        }
    }
}