namespace ShapeIntersect
{
    public class Rectangle : Shape
    {
        public Vector2 center;
        public Vector2 extents;

        public Rectangle(float centerX, float centerY, float extentsX, float extentsY, int id)
        {
            this.center = new Vector2(centerX, centerY);
            this.extents = new Vector2(extentsX, extentsY);
            this.id = id;
        }

        public Rectangle(float centerX, float centerY, float extentsX, float extentsY)
        {
            this.center = new Vector2(centerX, centerY);
            this.extents = new Vector2(extentsX, extentsY);
        }

        public Rectangle(Vector2 center, Vector2 extents, int id)
        {
            this.center = center;
            this.extents = extents;
            this.id = id;
        }

        public Rectangle(Vector2 center, Vector2 extents)
        {
            this.center = center;
            this.extents = extents;
        }

        public Vector2 minCorner => new Vector2(center.x - extents.x, center.y - extents.y);
        public Vector2 maxCorner => new Vector2(center.x + extents.x, center.y + extents.y);
        public Segment[] Sides()
        {
            Vector2 rightBot = new Vector2(maxCorner.x, minCorner.y);
            Vector2 leftUp = new Vector2(minCorner.x, maxCorner.y);
            Segment up = new Segment(leftUp, maxCorner, 0);
            Segment right = new Segment(maxCorner, rightBot);
            Segment down = new Segment(rightBot, minCorner);
            Segment left = new Segment(minCorner, leftUp);

            Segment[] sides = { up, right, down, left };
            return sides;
        }

        // Returns if a point is contained inside a Rectangle
        // assuming the rect is axis-aligned
        public bool Contains(Vector2 point)
        {
            return (
                point.x >= minCorner.x &&
                point.y >= minCorner.y &&
                point.x <= maxCorner.x &&
                point.y <= maxCorner.y
            );
        }

        public bool Contains(Rectangle rect) => Contains(rect.maxCorner) && Contains(rect.minCorner);

        public override Rectangle Bounds() => this;


        public override string ToString() => $".{center} <->{extents}";
    }
}