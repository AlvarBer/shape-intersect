namespace ShapeIntersect
{
    public class Circle : Shape
    {
        public Vector2 center;
        public float radius;

        public Circle(Vector2 center, float radius, int id)
        {
            this.center = center;
            this.radius = radius;
            this.id = id;
        }

        public Circle(float centerX, float centerY, float radius, int id)
        {
            this.center = new Vector2(centerX, centerY);
            this.radius = radius;
            this.id = id;
        }

        public override string ToString() => $".{center} ->{radius}";

        public override Rectangle Bounds() => new Rectangle(center, new Vector2(radius, radius));
    }
}